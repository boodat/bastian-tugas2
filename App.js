import Login from "./src/screen/Login";
import Daftar from "./src/screen/Daftar";
import Reset from "./src/screen/ResPassword";
import MainUser from "./src/screen/Masuk";
import * as React from 'react';
import { Image, StyleSheet, Text, TextInput, View, TouchableOpacity, ImageBase, Button } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="home"
      screenOptions={{
        headerShown: false
      }}>
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="MainUser" component={MainUser} />
        <Stack.Screen name="Daftar" component={Daftar} />
        <Stack.Screen name="Reset" component={Reset} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}