import * as React from 'react';
import { Image, StyleSheet, Text, TextInput, View, TouchableOpacity, ImageBase, Button } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

export default function Login({ navigation }) {
  return (
    <View style={styles.container}>
    <View style ={styles.content}>
      <View >
        <Image source={require('../image/unnamed.png')} style ={{
          resizeMode: 'contain',
          height: 70,
          width: 200,
          alignItems: "center",
          alignSelf : 'center',
        }}></Image>
        <View>
          <Text style={{
            alignItems : 'center',
            fontSize: 21,
            marginVertical: 10,
          }}>Please sign in to continue</Text>
        </View>
        <View style={styles.loginField}>
        <TextInput placeholder="Username" style={styles.textInput}></TextInput>
        </View>
        <View style={styles.loginField}>
            <TextInput placeholder="**********" style={styles.textInput}></TextInput>
        </View>
        <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate('MainUser')}
      >
        <Text style={{
            alignItems : 'center',
            fontSize: 17,
            color: 'white',
          }}>Sign In</Text>
      </TouchableOpacity>
      <TouchableOpacity
      onPress={() => navigation.navigate('Reset')}
      >
        <Text style={{
            alignItems : 'center',
            textAlign: "center",
            fontSize: 14,
            color: '#2E3283',
          }}>Forgot Password</Text>
      </TouchableOpacity>

      <View >
        
        <Text style={{
            textAlign: "center",
            fontSize: 14,
            color: '#2E3283',
            marginTop: 30,
            marginBottom: 20,
          }}>___________    Login with    ___________</Text>
        
      </View>
      <Text style={{
        fontSize: 13,
        marginVertical: 40,
        textAlign: "center",
      }}>App Version           2.8.3 </Text>
        
      </View>
      
    </View>
    <View style={styles.footer}>
      <Text
      >Don't have account?</Text>
      <TouchableOpacity
      onPress={() => navigation.navigate('Daftar')}>
        <Text>  Sign Up</Text>
      </TouchableOpacity>
    </View>
    
  </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex : 1,
    backgroundColor: '#F4F4F4',
    

  },
  content : {
    // backgroundColor: '#edf5fa',
    flex: 1,
    justifyContent: "center",
    margin: 70,
    // alignItems : 'center',
  },
  textInput : {
    backgroundColor: 'white',
    paddingLeft: 20,
    borderColor : 'black',
    borderRadius: 10,
    fontSize: 15,
  },
  loginField : {
    justifyContent: "center",
    marginHorizontal: -35,
    marginVertical: 10,  
  },
  button: {
    borderRadius: 10,
    fontSize: 15,
    alignItems: "center",
    backgroundColor: "#2E3283",
    marginHorizontal: -35,
    marginVertical: 10,
    padding: 15,
  },
  loginWith: {
    flexDirection: "row",
    // display: "flex",
    justifyContent: "space-around",
  },
  loginLogo: {
    resizeMode: 'contain',
    width: 50,
    height: 50,
  },
  footer: {
    display: 'flex',
    flexDirection: 'row',
    alignSelf: 'center',
    backgroundColor: 'white',
    // marginBottom: ,
    paddingHorizontal: 120,
    paddingVertical: 10,
  }


});