import * as React from 'react';
import { Image, StyleSheet, Text, TextInput, View, TouchableOpacity, ImageBase, Button } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';


export default function Daftar({ navigation }) {
  return (
    <View style={styles.container}>
    <View style ={styles.content}>
      <View >
      <Image source={require('../image/unnamed.png')} style ={{
          resizeMode: 'contain',
          height: 70,
          width: 200,
          alignItems: "center",
          alignSelf : 'center',
        }}></Image>
        <View>
          <Text style={{
            textAlign: 'center',
            fontSize: 21,
            marginVertical: 10,
          }}>Create an account</Text>
        </View>
        <View style={styles.loginField}>
        <TextInput placeholder="Username" style={styles.textInput}></TextInput>
        </View>
        <View style={styles.loginField}>
            <TextInput placeholder="Email" style={styles.textInput}></TextInput>
        </View>
        <View style={styles.loginField}>
            <TextInput placeholder="Phone" style={styles.textInput}></TextInput>
        </View>
        <View style={styles.loginField}>
            <TextInput placeholder="Password" style={styles.textInput}></TextInput>
        </View>
        <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate('Login')}
      >
        <Text style={{
            alignItems : 'center',
            fontSize: 17,
            color: 'white',
          }}>Sign In</Text>
      </TouchableOpacity>
        
      </View>
      
    </View>
    <View style={styles.footer}>
      <Text>Already have account?</Text>
      <TouchableOpacity
      onPress={() => navigation.navigate('Login')}>
        <Text>  Sign Up</Text>
      </TouchableOpacity>
    </View>
    
  </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex : 1,
    backgroundColor: '#F4F4F4',
    

  },
  content : {
    // backgroundColor: '#edf5fa',
    flex: 1,
    justifyContent: "center",
    margin: 70,
    // alignItems : 'center',
  },
  textInput : {
    backgroundColor: 'white',
    paddingLeft: 20,
    borderColor : 'black',
    borderRadius: 10,
    fontSize: 15,
  },
  loginField : {
    justifyContent: "center",
    marginHorizontal: -35,
    marginVertical: 10,  
  },
  button: {
    borderRadius: 10,
    fontSize: 15,
    alignItems: "center",
    backgroundColor: "#2E3283",
    marginHorizontal: -35,
    marginVertical: 10,
    padding: 15,
  },
  loginWith: {
    flexDirection: "row",
    // display: "flex",
    justifyContent: "space-around",
  },
  loginLogo: {
    resizeMode: 'contain',
    width: 50,
    height: 50,
  },
  footer: {
    display: 'flex',
    flexDirection: 'row',
    alignSelf: 'center',
    backgroundColor: 'white',
    // marginBottom: ,
    paddingHorizontal: 120,
    paddingVertical: 10,
    
  }


});